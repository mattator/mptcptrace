#include "graph.h"
#include "csvExporter.h"


void csv_verticalLine(FILE* f, unsigned int x, unsigned int y, unsigned long h, int color){}

void csv_verticalLineTime(FILE* f, struct timeval tsx, unsigned int y, unsigned int h, int color, int reinject){
	fprintf(f,"%li.%06li,%u,%i,1,%u,%i\n",tsx.tv_sec, tsx.tv_usec,y,color,y+h,reinject);
}
void csv_diamondTime(FILE *f, struct timeval tsx, unsigned int y, int color){
	fprintf(f,"%li.%06li,%u,%i,0,0,-1\n",tsx.tv_sec, tsx.tv_usec,y,color);
}
void csv_diamondTimeDouble(FILE *f, struct timeval tsx, double y, int color){
	fprintf(f,"%li.%06li,%f,%i,0,0\n",tsx.tv_sec, tsx.tv_usec,y,color);
}
void csv_textTime(FILE *f, struct timeval tsx, unsigned int y, char* text, int color){
	//TODO
}

void csv_writeHeader(FILE *f,char *way, char* title, char *xtype, char *ytype, char * xlabel, char *ylabel){
}
void csv_writeFooter(FILE *f,char *way, char* title, char *xtype, char *ytype, char * xlabel, char *ylabel){
}

FILE* csv_openGraphFile(char *name, int id, int way){
	char str[42];
	sprintf(str,"%s_%s_%d.csv",wayString[way],name,id);
	return fopen(str,"w");
}
void csv_writeSeries(FILE *f, char *type, char *name){}
