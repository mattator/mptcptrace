
void csv_verticalLine(FILE* f, unsigned int x, unsigned int y, unsigned long h, int color);
void csv_verticalLineTime(FILE* f, struct timeval tsx, unsigned int y, unsigned int h, int color, int reinject);
void csv_diamondTime(FILE *f, struct timeval tsx, unsigned int y, int color);
void csv_diamondTimeDouble(FILE *f, struct timeval tsx, double y, int color);
void csv_textTime(FILE *f, struct timeval tsx, unsigned int y, char* text, int color);
void csv_writeHeader(FILE *f,char *way, char* title, char *xtype, char *ytype, char * xlabel, char *ylabel);
void csv_writeFooter(FILE *f,char *way, char* title, char *xtype, char *ytype, char * xlabel, char *ylabel);
FILE* csv_openGraphFile(char *name, int id, int way);
void csv_writeSeries(FILE *f, char *type, char *name);
